
module.exports = {
    sections: {
        ClientHomeGMDPage: {
            selector: 'body',
            elements: {
                HomeHeader: {locateStrategy: 'xpath', selector: "//a[contains(text(),'Home')]"},
                ClientsHeader: {locateStrategy: 'xpath', selector: "//h2[contains(text(),'Clients')]"},
                ClientSearchTextbox: {locateStrategy: 'xpath', selector: "//span[contains(text(),'Filter:')]/../input"},
                MainHeader:{locateStrategy: 'xpath', selector: "//div[contains(text(),'This page can’t be displayed')]"},
            }
        }
        //HttpsErrorSecurityPage: {
        //  selector: 'body',
        // elements:{
        //        MainHeader:{locateStrategy: 'xpath', selector: "//h1[contains(text(),'This page can’t be displayed')]"},
        //  }
        //}
    }
}