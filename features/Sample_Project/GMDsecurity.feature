
Feature: GMD security testcases

  @GMDSecurity @GMDBypassAuthorization
  Scenario:  Testing for bypassing authentication schema
    Given  User opens the Browser and launch the GMD Application
    When   User Clicks on any client other than the test client
    Then   User should not be able to view the groups page for that client

  @GMDSecurity @GMDHTTPStrictTransport
  Scenario: To verify the security of GMD with HTTP
    Given  User launch the GMD Application using http
    Then   User change the GMD url to https and should not able to launch the application

  @GMDSecurity @GMDClickJacking1
  Scenario: To verify whether the GMD application opens in Iframe.
    Given    Open the GMDIframe- HTML file in IE browser
    Then     Check the GMD application is vulnerable or not

  @GMDSecurity @GMDServerDown
  Scenario: To verify when the server is down, the program code must not be displayed.
    Given    Make sure the server is down and open the GMD Application
    Then     The program code for GMD Application must not be displayed
