Feature: VIP Admin Security Test cases

  @Security @BypassAuthentication
  Scenario:  Testing for bypassing authentication schema
    Given  User opens the Browser and launch the VIP Admin Application
    When   User Clicks on any client other than SMLMKT
    Then   User should be on the Carrier Dashboard Page itself

  @Security @HTTPStrictTransport
  Scenario: To verify the security of VIP Admin with HTTP
    Given  User launch the VIP Admin Application using http
    Then   User change the url to https and should not able to launch the application

  @Security @ClickJacking1
  Scenario: To verify whether the VIP admin application opens in Iframe.
   Given    Open the Iframe- HTML file in IE browser
   Then     Check the VIP Admin application is vulnerable or not

  @Security @ServerDown
  Scenario: To verify when the server is down, the program code must not be displayed.
   Given    Make sure the server is down and open the VIPAdmin Application
   Then     The program code must not be displayed

