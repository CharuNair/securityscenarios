/**
 * Created by Charu-Nair on 12/12/2017.
 */

var data = require('../../TestResources/AdminsecurityData.js');
var BrowserData = require('../../TestResources/AdminsecurityData.js');
var Objects = require(__dirname + '/../../repository/AdminsecurityPages.js');

var ks = require('node-key-sender');
//var path = require('../../Downloads');
var fs = require('fs');

function initializePageObjects(browser, callback) {
    var VIPAdminPage = browser.page.AdminsecurityPages();
    DashboardPage = VIPAdminPage.section.ClientHomeVIPAdminPage;
    //SecurityPage=  VIPAdminPage.section.HttpsErrorSecurityPage;
    callback();
}

module.exports = function() {

    this.Given(/^User opens the Browser and launch the VIP Admin Application$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironments"];
        console.log('Test Environment: ' + execEnv);
        console.log('Browser: ' + BrowserData.BrowserInTest);
        if (execEnv.toUpperCase() == "QA") {
            URL = data.VIPAdminURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                DashboardPage.waitForElementVisible('@HomeHeader', data.LongwaitVIPAdmin)
                    .waitForElementVisible('@ClientsHeader', data.LongwaitVIPAdmin)

            });
        }
    });

    this.When(/^User Clicks on any client other than SMLMKT$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        console.log('Browser: ' + BrowserData.BrowserInTest);
        if (execEnv.toUpperCase() == "QA") {
            DashboardPage.waitForElementVisible('@ClientSearchTextbox', data.LongwaitVIPAdmin)
                .clearValue('@ClientSearchTextbox')
                .setValue('@ClientSearchTextbox', data.ClientNameVIPAdmin);
            browser.pause(data.ElementaryPause);

            //click on the search CLient name
            browser.useXpath().click("//a[contains(text(),'" + data.ClientNameVIPAdmin + "')]");
        }

    });

    this.Then(/^User should be on the Carrier Dashboard Page itself$/, function(){
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        console.log('Browser: ' + BrowserData.BrowserInTest);
        if (execEnv.toUpperCase() == "QA") {
            DashboardPage.waitForElementVisible('@HomeHeader', data.LongwaitVIPAdmin)
                .waitForElementVisible('@ClientsHeader', data.LongwaitVIPAdmin)
        }
    });

    this.Given(/^User launch the VIP Admin Application using http$/,function(){
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironments"];
        console.log('Test Environment: ' + execEnv);
        console.log('Browser: ' + BrowserData.BrowserInTest);
        if (execEnv.toUpperCase() == "QA") {
            URL = data.VIPAdminURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                DashboardPage.waitForElementVisible('@HomeHeader', data.LongwaitVIPAdmin)
                    .waitForElementVisible('@ClientsHeader', data.LongwaitVIPAdmin)

            });
        }
    });

    this.Then(/^User change the url to https and should not able to launch the application$/,function(){
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironments"];
        console.log('Test Environment: ' + execEnv);
        console.log('Browser: ' + BrowserData.BrowserInTest);
        if (execEnv.toUpperCase() == "QA") {
            URL = data.VIPAdminURLhttps;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                DashboardPage.waitForElementVisible('@MainHeader', data.LongwaitVIPAdmin)

            });
        }
    });

    this.Given(/^Open the Iframe- HTML file in IE browser$/,function(){
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironments"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            URL = data.VipAdminIframeFile;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.pause(10000);
            });
        }
    });

    this.Then(/^Check the VIP Admin application is vulnerable or not$/,function(){
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironments"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            var VulnerableText="//p[contains(text(),'This website is vulnerable to Clickjacking')]";
            var Adminsite="//span[contains(text(),'VENDOR INTEGRATION PROCESS')]";
            var Homepage= "//h2[contains(text(),'Clients']";
            browser.useXpath().waitForElementNotPresent(VulnerableText, data.longWait);
            browser.pause(5000)
                .frame('ITest')
            browser.useXpath().waitForElementNotPresent(Adminsite, data.longWait)
            browser.useXpath().waitForElementNotPresent(Homepage, data.longWait);

        }
    });

    this.Given(/^Make sure the server is down and open the VIPAdmin Application$/,function(){
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironments"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            URL = data.VIPAdminURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.pause(10000);
            });
        }
    });

    this.Then(/^The program code must not be displayed$/,function(){
        //var URL;
        browser = this;
        var execEnv = data["TestingEnvironments"];
        console.log('Test Environment: ' + execEnv);
        console.log('Browser: ' + BrowserData.BrowserInTest);
        if (execEnv.toUpperCase() == "QA") {
            initializePageObjects(browser, function () {

                DashboardPage.waitForElementNotPresent('@HomeHeader', data.LongwaitVIPAdmin)
                    .waitForElementNotPresent('@ClientsHeader', data.LongwaitVIPAdmin)

            });
        }
    });

    
}
