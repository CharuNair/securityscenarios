
var data = require('../../TestResources/GMDsecurityData.js');
var BrowserData = require('../../TestResources/GMDsecurityData.js');
var Objects = require(__dirname + '/../../repository/GMDsecurityPages.js');

var ks = require('node-key-sender');
//var path = require('../../Downloads');
var fs = require('fs');

function initializePageObjects(browser, callback) {
    var VIPAdminPage = browser.page.GMDsecurityPages();
    GMDPage = VIPAdminPage.section.ClientHomeGMDPage;
    //SecurityPage=  VIPAdminPage.section.HttpsErrorSecurityPage;
    callback();
}


module.exports = function() {

    this.Given(/^User opens the Browser and launch the GMD Application$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironments"];
        console.log('Test Environment: ' + execEnv);
        console.log('Browser: ' + BrowserData.BrowserInTest);
        if (execEnv.toUpperCase() == "QA") {
            URL = data.GMDURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                GMDPage.waitForElementVisible('@HomeHeader', data.LongwaitGMD)
                    .waitForElementVisible('@ClientsHeader', data.LongwaitGMD)

            });
        }
    });

    this.When(/^User Clicks on any client other than the test client$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironments"];
        console.log('Test Environment: ' + execEnv);
        console.log('Browser: ' + BrowserData.BrowserInTest);
        if (execEnv.toUpperCase() == "QA") {
            GMDPage.waitForElementVisible('@ClientSearchTextbox', data.LongwaitGMD)
                .clearValue('@ClientSearchTextbox')
                .setValue('@ClientSearchTextbox', data.ClientNameGMD);
            browser.pause(data.ElementaryPause);

            //click on the search CLient name
            browser.useXpath().click("//a[contains(text(),'" + data.ClientNameGMD + "')]");
        }

    });

    this.Then(/^User should not be able to view the groups page for that client$/, function(){
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironments"];
        console.log('Test Environment: ' + execEnv);
        console.log('Browser: ' + BrowserData.BrowserInTest);
        if (execEnv.toUpperCase() == "QA") {
            GMDPage.waitForElementVisible('@HomeHeader', data.LongwaitGMD)
                .waitForElementVisible('@ClientsHeader', data.LongwaitGMD)
        }
    });

    this.Given(/^User launch the GMD Application using http$/,function(){
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironments"];
        console.log('Test Environment: ' + execEnv);
        console.log('Browser: ' + BrowserData.BrowserInTest);
        if (execEnv.toUpperCase() == "QA") {
            URL = data.GMDURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                GMDPage.waitForElementVisible('@HomeHeader', data.LongwaitGMD)
                    .waitForElementVisible('@ClientsHeader', data.LongwaitGMD)

            });
        }
    });

    this.Then(/^User change the GMD url to https and should not able to launch the application$/,function(){
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironments"];
        console.log('Test Environment: ' + execEnv);
        console.log('Browser: ' + BrowserData.BrowserInTest);
        if (execEnv.toUpperCase() == "QA") {
            URL = data.GMDURLhttps;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                GMDPage.waitForElementVisible('@MainHeader', data.LongwaitGMD)

            });
        }
    });

    this.Given(/^Open the GMDIframe- HTML file in IE browser$/,function(){
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironments"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            URL = data.GMDIframeFile;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.pause(10000);
            });
        }
    });

    this.Then(/^Check the GMD application is vulnerable or not$/,function(){
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironments"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            var VulnerableText="//p[contains(text(),'This website is vulnerable to Clickjacking')]";
            var GMDsite="//a[contains(text(),'BENEFITS')]";
            var Homepage= "//h2[contains(text(),'Clients']";
            browser.useXpath().waitForElementNotPresent(VulnerableText, data.longWait);
            browser.pause(5000)
                .frame('ITest')
            browser.useXpath().waitForElementNotPresent(GMDsite, data.longWait)
            browser.useXpath().waitForElementNotPresent(Homepage, data.longWait);

        }
    });

    this.Given(/^Make sure the server is down and open the GMD Application$/,function(){
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironments"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            URL = data.GMDURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.pause(10000);
            });
        }
    });

    this.Then(/^The program code for GMD Application must not be displayed$/,function(){
        //var URL;
        browser = this;
        var execEnv = data["TestingEnvironments"];
        console.log('Test Environment: ' + execEnv);
        console.log('Browser: ' + BrowserData.BrowserInTest);
        if (execEnv.toUpperCase() == "QA") {
            initializePageObjects(browser, function () {

                GMDPage.waitForElementNotPresent('@HomeHeader', data.LongwaitGMD)
                    .waitForElementNotPresent('@ClientsHeader', data.LongwaitGMD)

            });
        }
    });


}
